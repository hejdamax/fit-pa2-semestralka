#ifndef TRANSFORM_H
#define TRANSFORM_H
#include <iostream>
#include <utility>
#include <vector>
#include <memory>
#include "CPngImg.h"
#include "CJpegImg.h"
#include "CImage.h"
#include "CTransformedImg.h"

class CTransform
{
    public:
        /// Flips the image horizontally.
        /// This means that the image is flipped from left to right. (What is on left will be on right.)
        /// @param[in] vector<shared_ptr<CImage>>::iterator& Iterator referring to the image that will be the source image for the transformation.
        /// @returns shared_ptr<CTransformedImg> wrapper around the source image with some extra parameters set.
        shared_ptr<CTransformedImg> flipHorizontal(vector<shared_ptr<CImage>>::iterator&);
        /// Flips the image vertically.
        /// This means that the image is flipped from top to bottom. (What is on top will be on bottom.)
        /// @param[in] vector<shared_ptr<CImage>>::iterator& Iterator referring to the image that will be the source image for the transformation.
        /// @returns shared_ptr<CTransformedImg> wrapper around the source image with some extra parameters set.
        shared_ptr<CTransformedImg> flipVertical(vector<shared_ptr<CImage>>::iterator&);
        /// Flips the image both horizontally and vertically.
        /// @param[in] vector<shared_ptr<CImage>>::iterator& Iterator referring to the image that will be the source image for the transformation.
        /// @returns shared_ptr<CTransformedImg> wrapper around the source image with some extra parameters set.
        shared_ptr<CTransformedImg> flipBoth(vector<shared_ptr<CImage>>::iterator&);
        /// Rotates the image by 90, 180 or 270 degrees.
        /// @param[in] vector<shared_ptr<CImage>>::iterator& Iterator referring to the image that will be the source image for the transformation.
        /// @returns shared_ptr<CTransformedImg> wrapper around the source image with some extra parameters set.
        shared_ptr<CTransformedImg> rotate(vector<shared_ptr<CImage>>::iterator&);
        /// Changes the brightness of the image. \n
        /// This means that the output image will be darker or lighter by the set percentage.
        /// @param[in] vector<shared_ptr<CImage>>::iterator& Iterator referring to the image that will be the source image for the transformation.
        /// @returns shared_ptr<CTransformedImg> wrapper around the source image with some extra parameters set.
        shared_ptr<CTransformedImg> changeBrightness(vector<shared_ptr<CImage>>::iterator&);
};
#endif /* TRANSFORM_H */