#include "CAsciiScale.h"

using namespace std;

void CAsciiScale::loadAscii(string &filename)
{
    string line;
    char asciiChar;
    char delimiter;
    char separator;
    int maxColorValue;
    int prevMaxColorValue = -1;
    ifstream asciiScale;
    asciiScale.open(filename);
    if(asciiScale.fail())
    {
        asciiScale.clear();
        throw("File does not exist");
    }
    getline(asciiScale, line);
    if(!asciiScale.eof())
    {
        throw("File has too many lines");
    }
    stringstream lineStream(line);
    while(lineStream)
    {
        lineStream >> asciiChar >> delimiter >> maxColorValue >> separator;
        if(!isprint(asciiChar))
        {
            throw("Character is non printable");
        }
        if(delimiter != ':')
        {
            throw("Delimiter not correct");
        }
        if(prevMaxColorValue >= maxColorValue)
        {
            throw("Color value is less than or equal to the last");
        }
        m_asciiScale.push_back({asciiChar, maxColorValue});
        if(separator != ',')
        {
            throw("Separator not correct");
        }
    }
    if(m_asciiScale.size() != 8)
    {
        throw("Ascii scale has the wrong number of characters");
    }

}

void CAsciiScale::displayCharacter(int pixelColor) const
{
    if (pixelColor <= m_asciiScale.at(0).second)
    {
    cout << m_asciiScale.at(0).first;
    }
    else if (pixelColor <= m_asciiScale.at(1).second)
    {
    cout << m_asciiScale.at(1).first;
    }
    else if (pixelColor <= m_asciiScale.at(2).second)
    {
    cout << m_asciiScale.at(2).first;
    }
    else if (pixelColor <= m_asciiScale.at(3).second)
    {
    cout << m_asciiScale.at(3).first;
    }
    else if (pixelColor <= m_asciiScale.at(4).second)
    {
    cout << m_asciiScale.at(4).first;
    }
    else if (pixelColor <= m_asciiScale.at(5).second)
    {
    cout << m_asciiScale.at(5).first;
    }
    else if (pixelColor <= m_asciiScale.at(6).second)
    {
    cout << m_asciiScale.at(6).first;
    }
    else if (pixelColor <= m_asciiScale.at(7).second)
    {
    cout << m_asciiScale.at(7).first;
    }
    else
    {
    cout << ' ';
    }
}

void CAsciiScale::printScale() const
{
    for(auto iter = m_asciiScale.begin(); iter != m_asciiScale.end(); iter++)
    {
        cout << iter -> first << ':' << iter -> second << " ";
    }
    cout << endl;
}