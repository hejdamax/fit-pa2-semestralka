#ifndef TRANSFORMED_IMG_H
#define TRANSFORMED_IMG_H
#include <iostream>
#include <ostream>
#include <memory>
#include "CImage.h"
#include <png.h>
#include <setjmp.h>

using namespace std;

class CTransformedImg: public CImage
{
    public:
        /// This is just a dummy function because the function in CImage is pure virtual.
        bool loadImage(const string &fileName ) {return true;};
        /// Combines the name of the source image with a given ending. The ending indicates which transformation was used.
        string createName(string ending) const;
        /// Calculates which pixel from the source image is mapped to the given position of the output image. \n
        /// This is done by multiplying the coordinate vector with the transformation matrix.
        /// @param[in] uint64_t x coordinate 
        /// @param[in] uint64_t y coordinate
        /// @returns pair representing the x y coordinates of the source image that are mapped to the input coordinates. 
        pair<uint64_t, uint64_t> computeXY(uint64_t, uint64_t) const;
        /// This method fills the CTransformedImg with the correct information.
        /// @param[in] tuple represents the transformation matrix.
        /// @param[in] shared_ptr<CImage>& The source image from which the output image is calculated.
        /// @param[in] string The ending indicating which transformation was used.
        /// @param[in] pair represents the height and width of the transformed image.
        /// @param[in] int If the default value -200 is not used this parameter contains the percentage of the brightness change.
        void setTransformationParameters(tuple<int, int, int, int> matrix, shared_ptr<CImage> & orignal_img, string ending, pair<uint64_t, uint64_t> & size, int brightness = -200);
    private:
        /// getpixel returns a grayscale representation of the color of given pixel.\n
        /// Input paramters are height and width in this order.
        int getPixel(uint64_t, uint64_t) const;
        /// Transformation parameters for x cordinate
        /// are a, b and c.\n
        /// Transformation parameters for y cordinate
        /// are d, e and f.\n
        struct CTransformMatrix
        {
    
            int a;
            int b;
            int d;
            int e;
        };
         /// This matrix holds the transformation parameters that calculate the x y cordinates of the given pixel.
        CTransformMatrix m_matrix;
        shared_ptr<CImage> m_originalImg;
        string m_ending;
        int m_brightness = 0;
};
#endif /* TRANSFORMED_IMG_H */