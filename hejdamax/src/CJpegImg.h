#ifndef JPG_H
#define JPG_H
#include <iostream>
#include <ostream>
#include <memory>
#include "CImage.h"
#include "jpeglib.h"
#include <setjmp.h>
using namespace std;

class CJpegImg: public CImage
{   
    public:
        ///Loads the image from a file to memory.
        ///@param[in] string& Path to jpeg image.
        /// @return bool Indicating whether the upload was completed correctly.
        bool loadImage(const string &fileName);
        /// Setup function of the jpeg library.
        void setupError();
        ~CJpegImg();
    private:
        /// Getpixel returns a grayscale representation of the color of the pixel in the given position.\n
        /// @param [in]  uint64_t y position
        /// @param [in]  uint64_t x position
        /// @return int from 0 to 255 that is then mapped to a character on the ASCII scale.
        int getPixel(uint64_t, uint64_t) const;

        /// Functions that are needed by the jpeg library.
        friend void jpegErrorHandling(j_common_ptr cinfo);
        /// Functions that are needed by jpeg library.
        struct jpegErrorManager 
        {
            struct jpeg_error_mgr pub;	/* "public" fields */
            jmp_buf setjmp_buffer;	/* for return to caller */
        };
        /// Ffunctions that are needed by jpeg library.
        jpegErrorManager jerr;
        struct jpeg_decompress_struct m_info;
        uint8_t *m_data;
        const string m_type = "jpeg";
};
#endif /* IMAGE_H */