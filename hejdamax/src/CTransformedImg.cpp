#include "CTransformedImg.h"

int CTransformedImg::getPixel(uint64_t y_position, uint64_t x_position) const
{
    pair<uint64_t, uint64_t> px = computeXY(x_position, y_position);
    int pixel = m_originalImg -> getPixel(px.second, px.first);

    // this changes the pixel brightness. If brightness is different than default this will add or subtract a percentage of the pixel color.
    pixel += (m_brightness/100.0 * pixel);
    // Setting the min value of pixel and max value of pixel
    if(pixel < 0)
    {
        pixel = 0;
    }
    if(pixel > 255)
    {
        pixel = 255;
    }
    return pixel;
}
string CTransformedImg::createName(string ending) const
{
    return m_originalImg -> getName() + ending;
}

pair<uint64_t, uint64_t> CTransformedImg::computeXY(uint64_t u, uint64_t v) const
{
    uint64_t x = 0;
    uint64_t y = 0;
    if (m_matrix.a == -1 || m_matrix.d == -1)
    {
        u = u - (m_width - 1);
    }
    if (m_matrix.e == -1 || m_matrix.b == -1)
    {
        v = v - (m_height - 1);
    }
    x = m_matrix.a * u + m_matrix.b * v;
    y = m_matrix.d * u + m_matrix.e * v;
    return make_pair(x, y);
}
void CTransformedImg::setTransformationParameters(tuple<int, int, int, int> matrix, shared_ptr<CImage> &orignal_img, string ending, pair<uint64_t, uint64_t> &size, int brightnes)
{
    m_matrix.a = get<0>(matrix);
    m_matrix.b = get<1>(matrix);
    m_matrix.d = get<2>(matrix);
    m_matrix.e = get<3>(matrix);

    m_originalImg = orignal_img;
    m_name = createName(ending);

    m_height = size.first;
    m_width = size.second;

    if(brightnes != -200)
    {
        m_brightness = brightnes;
    }
}