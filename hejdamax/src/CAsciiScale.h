#ifndef ASCII_H
#define ASCII_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

/**
* This class contains all the functions that are needed for loading and displaying the correct character of the ASCII scale.
* 
* The ASCII scale is represented by a txt file containing one line.
* On that line there are 8 pairs separated by a comma. These values have to be arranged from the darkest to the lightest (this means from the lowest color value to the highest).
* The pair is formed by an ASCII char and max color that will still be used to display this char, separated by a colon (e.g. M:144).
* The color values are from 0 (black) to 255 (white) where 0 is the darkest and 255 is the lightest.
* All chars in the scale have to be printable. If they aren't the rendering will not work.
* 
* Example (this is the default that is used):
* M:144,N:156,F:168,V:181,|:197,*:218,::237,.:245
* 
*/

class CAsciiScale
{
    public:
        /// Uploads a new ASCII scale if the input file has the correct format.
        /// @param[in] string& path to file.
        void loadAscii(string &fileName);
        /// Prints the correct ASCII character to stdout based on the pixel color.
        /// @param[in] int represents the color of the pixel. It is a number from 0 to 255 where 0 is black and 255 is white.
        void displayCharacter(int pixelColor) const;
        /// Prints the scale. Used in selectors.
        void printScale() const;
    private:
        vector<pair<char, int>> m_asciiScale;
};

#endif /* ASCII_H */