#include "CJpegImg.h"

void jpegErrorHandling(j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  CJpegImg::jpegErrorManager *myerr = (CJpegImg::jpegErrorManager *)cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message)(cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}

void CJpegImg::setupError()
{
  m_info.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = jpegErrorHandling;
}

bool CJpegImg::loadImage(const string &fileName)
{
  FILE *fp;
  if (!(fp = fopen(fileName.c_str(), "rb")))
  {
    cout << "Cant open" << endl;
    return false;
  }
  /* Step 1: allocate and initialize JPEG decompression object */
  setupError();
  /* Establish the setjmp return context for my_error_exit to use. */
  if (setjmp(jerr.setjmp_buffer))
  {
    jpeg_destroy_decompress(&m_info);
    fclose(fp);
    cout << "error" << endl;
    return false;
  }
  jpeg_create_decompress(&m_info);

  /* Step 2: specify data source (eg, a file) */
  jpeg_stdio_src(&m_info, fp);

  /* Step 3: read file parameters with jpeg_read_header() */
  (void)jpeg_read_header(&m_info, TRUE);

  jpeg_start_decompress(&m_info);
  int row_stride = m_info.output_width * m_info.output_components;
  /* Make a one-row-high sample array that will go away when done with image */

  int w = m_info.output_width;
  int h = m_info.output_height;

  uint8_t *data = new uint8_t[w * h * 3];
  long counter = 0;

  // step 4, read the image line by line
  while (m_info.output_scanline < m_info.output_height)
  {
    JSAMPROW buffer[1] = {(JSAMPROW)(data + counter)};
    jpeg_read_scanlines(&m_info, buffer, 1);
    counter += row_stride;
  }
  /* Step 5: Finish decompression */

  (void)jpeg_finish_decompress(&m_info);
  /* Step 6: Release JPEG decompression object */

  jpeg_destroy_decompress(&m_info);

  fclose(fp);
  m_data = data;
  m_width = w;
  m_height = h;
  m_name = fileName;
  cout << "Upload completed." << endl;
  return true;
}

int CJpegImg::getPixel(uint64_t y_position, uint64_t x_position) const
{
  return ((int)m_data[(y_position * m_width * 3) + (x_position * 3) + 0] +
          (int)m_data[(y_position * m_width * 3) + (x_position * 3) + 1] +
          (int)m_data[(y_position * m_width * 3) + (x_position * 3) + 2])/3;
}

CJpegImg::~CJpegImg()
{
  if(m_data)
  {
    delete[] m_data;
  }
}
